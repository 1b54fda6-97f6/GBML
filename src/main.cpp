/*
=========================================================================
---[GBML - main.cpp]---
This file is part of Gaming Backup Multitool for Linux (or GBML for short).
Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license.
See the accompanying COPYING file for more details.
=========================================================================
*/

//-----------------------------------------------------------------------------------------------------------
// INCLUDED LIBRARIES
//-----------------------------------------------------------------------------------------------------------

// Standard C++/Qt libs
#include <QApplication>
#include <QDir>
#include <QMessageBox>

// Program-specific libs
#include "db.h"
#include "mainmenu.h"
#include "progressmenu.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

/*  LIST OF EXIT CODES FOR REFERENCE:
 *  0 - Program executed successfully
 *  1 - Database is missing
 *  2 - Steam folder not found
 */

//-----------------------------------------------------------------------------------------------------------
// STARTUP
//-----------------------------------------------------------------------------------------------------------

QApplication *App;  // Pointer for application
MainWindow *Window; // Pointer for main window

// The main man
int main(int argc, char *argv[]){
    QApplication a(argc, argv); App = &a;   // Creating a new application
    MainWindow w; Window = &w;              // Creating the main window

    // Program only opens if the database is present AND Steam folder is found
    if (QFile(QString::fromStdString(DB::DBPath)).exists()){
        if (DB::CheckSteamInstall()){
            Window->show();         // Show the UI
            MainMenu::LoadPaths();  // Read config file for saved folders
            MainMenu::LockUnlock(); // Lock some buttons
            return App->exec();     // Execute the program
        } else {
            QMessageBox err;
            err.critical(0, "Error", "Steam folder not found, aborting");
            err.setFixedSize(500,200);
            return 2;
        }


    // If database is not found, show an error dialog and return error code 1
    } else {
        QMessageBox err;
        err.critical(0, "Error", "Database is missing");
        err.setFixedSize(500,200);
        return 1;
    }
}
