/*
=========================================================================
---[GBML - Database functionalities (header file)]---
This file is part of Gaming Backup Multitool for Linux (or GBML for short).
Gaming Backup Multitool for Linux is available under the GNU GPL v3.0 license.
See the accompanying COPYING file for more details.
=========================================================================
*/

#ifndef DB_H
#define DB_H

#include <cstddef>
#include <string>
#include <QString>
#include <QDir>
#include "sqlite3.h"

class DB {
    public:
        DB();
        static QDir GBMLConfigPath;
        static std::string SteamPath;
        static std::string UserPath;
        static std::string DBPath;
        static bool CheckSteamInstall();
        static std::string FetchGameInfo(std::string query);
        static QStringList FetchColumn(std::string column);
        static void ReplaceLabels(std::string &string);
        static void ReplaceLabels(QString &string);
};

#endif // DB_H
